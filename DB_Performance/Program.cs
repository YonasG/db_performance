using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Collections;
using ClickHouse.Ado;
using Microsoft.AspNetCore.Connections;
using DB_Performance.Core;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


//... ���-�� ��� ...
List<MyPersistableObject> list = new();
// ���������� ������ ����������� ��������
list.Add(new MyPersistableObject());


using (var cnn = ConnectionHandlerClickHouse.GetConnection())
{
	var command = cnn.CreateCommand();
	command.CommandText = "INSERT INTO test.visits VALUES (2, 40.2, 'http://example1.com', '2019-01-03 10:01:01');";
	//command.Parameters.Add(new ClickHouseParameter
	//{
	//	ParameterName = "bulk",
	//	Value = list
	//});
	command.ExecuteNonQuery();
}


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();


class MyPersistableObject : IEnumerable
{
	public string MyStringField;
	public DateTime MyDateField;
	public int MyIntField;

	//���������� � ������� return ������ ��������������� ���������� � ������� ����� � SQL INSERT
	public IEnumerator GetEnumerator()
	{
		yield return MyDateField;
		yield return MyDateField;
		yield return MyStringField;
		yield return (ushort)MyIntField;
	}
}