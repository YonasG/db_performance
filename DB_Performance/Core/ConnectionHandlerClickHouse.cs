﻿using ClickHouse.Ado;

namespace DB_Performance.Core
{
    internal class ConnectionHandlerClickHouse
    {
        public static ClickHouseConnection GetConnection(
            string cstr = "Compress=False;BufferSize=32768;SocketTimeout=10000;CheckCompressedHash=False;Encrypt=False;Compressor=lz4;Host=192.168.0.78;Port=9000;User=default")
        {
            var settings = new ClickHouseConnectionSettings(cstr);
            var cnn = new ClickHouseConnection(settings);
            cnn.Open();
            return cnn;
        }
    }
}
